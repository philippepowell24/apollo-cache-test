import logo from './logo.svg';
import './App.css';
import { gql } from 'graphql-tag';
import React from 'react';
import { useQuery } from '@apollo/client';

const COUNTRY = gql`
  query Country($code: ID!) {
    country(code: $code) @client {
      code
      name
      emoji
      nameWithEmoji
    }
  }
`;

const COUNTRIES = gql`
  query Countries {
    countries {
      code
      name
      emoji
      nameWithEmoji @client
    }
  }
`;

function App() {
  const [code, setCode] = React.useState('US');

  const {
    data: countryData,
    loading: countryLoading,
    error: countryError,
  } = useQuery(COUNTRY, {
    variables: {
      code,
    },
  });
  const { data, loading, error } = useQuery(COUNTRIES);
  console.log(data);
  return (
    <div className="App">
      {countryError && <p>Something went wrong...</p>}
      {error && <p>Something went wrong...</p>}
      {countryLoading && <p>Loading...</p>}
      {loading && <p>Loading...</p>}
      {countryData && <p>{countryData.country.nameWithEmoji}</p>}
      <input value={code} onChange={(e) => setCode(e.target.value)} />
      {data && (
        <ul>
          {data?.countries?.map((e) => (
            <li key={e.name}>{e.nameWithEmoji}</li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default App;
